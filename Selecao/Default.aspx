﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Selecao.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            teste git
            <asp:textbox runat="server" ID="txtNumero"></asp:textbox> <asp:Button ID="btnVerificar" runat="server" Text="Verificar" OnClick="btnVerificar_Click"/>
            <asp:Label ID="lblResultado" runat="server" Text=""></asp:Label>
        </div><br /><br />
        <div>         
            <asp:GridView ID="GridView" runat="server" AutoGenerateColumns="false" OnRowDataBound="GridView_RowDataBound" OnDataBinding="GridView_DataBinding">
           <Columns>
               <asp:BoundField  HeaderText="Nome" DataField="Nome"/>
               <asp:BoundField  HeaderText="Cargo" DataField="Cargo" />
               <asp:BoundField  HeaderText="Data de Nascimento"  DataField="DataNasc"/>
               <asp:BoundField  HeaderText="Telefone" DataField="Telefone" />
               <asp:BoundField  HeaderText="Dias Trabalhados" DataField="DiasTrabalhados" />
               <asp:BoundField  HeaderText="Salario" DataField="Salario" />
           </Columns>
       </asp:GridView>
        </div>
    </form>
</body>
</html>
