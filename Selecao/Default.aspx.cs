﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Selecao
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                GridView.DataBind();
            }
        }

        //Este botão é de outra funcionalidade
        protected void btnVerificar_Click(object sender, EventArgs e)
        {
            int numero = int.Parse(txtNumero.Text);

            //verificar se é feliz
            bool feliz = false;
            int primeira = numero * numero;
            int valor = 0;
            if (primeira == 1)
            {
                feliz = true;
            }
            else
            {
                int i = 1;
                while (i < 99)
                {
                    valor = valor + primeira;
                    string numeros = valor.ToString();
                    int[] intArray = numeros.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
                    int multi = 0;
                    for (int j = 0; j < intArray.Count(); j++)
                    {
                        multi = multi + (intArray[j] * intArray[j]);
                    }
                    if (multi == 1)
                    {
                        feliz = true;
                        break;
                    }
                    i++;
                    primeira = 0;
                    valor = multi;
                }
            }
            //verifcar se é sortudo
            bool sortudo = false;
            List<int> primeroSortudos = new List<int>();

            for (int k = 1; k <= numero; k++)
            {
                if (k % 2 != 0)
                    primeroSortudos.Add(k); //impares
            }
            List<int> posicoes = new List<int>();

            for (int p = 1; p < primeroSortudos.Count; p++)
            {
                int valordoMultiplo = primeroSortudos[p];
                for (int m = 1; m < primeroSortudos.Count; m++)
                {
                    posicoes.Add(valordoMultiplo);
                    int nnn = primeroSortudos[p];
                    valordoMultiplo = valordoMultiplo + nnn;
                }
                foreach (int indice in posicoes.OrderByDescending(v => v))
                {
                    if (primeroSortudos.Count > indice)
                        primeroSortudos.RemoveAt(indice - 1);
                }
                posicoes.Clear();
            }
            if (primeroSortudos.Contains(numero))
            {
                sortudo = true;
            }
            string resultado = string.Empty;
            if (sortudo)           
                resultado = "Número Sortudo";           
            else           
                resultado = "Número Não-Sortudo";            
            if (feliz)          
                resultado += " e Feliz.";           
            else          
                resultado += " e Não-Feliz.";   

            lblResultado.Text = resultado;
        }

        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var itens = (Funcionario)e.Row.DataItem;
            }
        }

        protected void GridView_DataBinding(object sender, EventArgs e)
        {
            List<Funcionario> listaFunc = new List<Funcionario>
                {
                    new Funcionario("Maria", "Desenvolvedora", DateTime.Parse("1985-08-15"), "22222222", 15, 2352.75),
                    new Funcionario("Joao", "Recepcionista", DateTime.Parse("1972-03-18"), "22225555", 3, 1550.35)
                };

            GridView.DataSource = listaFunc;
        }
    }

    public class Funcionario
    {
        public string Nome { get; set; }
        public string Cargo { get; set; }
        public DateTime DataNasc { get; set; }
        public string Telefone { get; set; }
        public int DiasTrabalhados { get; set; }
        public double Salario { get; set; }

        public Funcionario(string nome, string cargo, DateTime dataNasc, string telefone, int diasTrabalhados, double salario)
        {
            Nome = nome;
            Cargo = cargo;
            DataNasc = dataNasc;
            Telefone = telefone;
            DiasTrabalhados = diasTrabalhados;
            Salario = salario;
        }
    }
}